﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarlo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label3.Hide();
            label4.Hide();
            label5.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Int64 decimale = Convert.ToInt64(textBox1.Text);
            Random random = new Random();
            Int64  circ = 0;
            Int64 itr = 0;
            double eps = 1 / (2*Math.Pow(10, decimale));
            progressBar1.Value = 0;
            label4.Hide();
            label5.Hide();
            double old_val = 20.0, new_val = 0.0;
            while(true)
            {
                double x = random.NextDouble();
                double y = random.NextDouble();
                double rez = Math.Pow(x, 2) + Math.Pow(y, 2);
                if (rez <= 1)
                {
                    circ++;
                }
                itr++;
                if(itr !=circ && circ!=0)
                new_val = 4*circ /(double)itr;
                if (Math.Abs(old_val-new_val) < eps)
                {
                    label4.Text = Convert.ToString(itr);
                    label5.Text = Convert.ToString(new_val);
                    label6.Text = Convert.ToString(eps);
                    label4.Visible = true;
                    label5.Visible = true;
                    break;
                }
                if (itr != circ && circ != 0)
                {
                    old_val = new_val;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Int64 iteracije = Convert.ToInt64(textBox2.Text);
            Random random = new Random();
            Int64  circ = 0;
            double pom = iteracije /(double) 100;
            progressBar1.Value = 0;
            progressBar1.Step = 1;
            progressBar1.Maximum = (int)iteracije;
            label3.Hide();
            for(int i=0;i<iteracije;i++)
            {
                double x = random.NextDouble();
                double y = random.NextDouble();
                double rez = Math.Pow(x, 2) + Math.Pow(y, 2);
                if (rez <= 1)
                {
                    circ++;
                }
                progressBar1.PerformStep();
            }
            double pi =4* circ / (double)iteracije;
            label3.Visible = true;
            label3.Text = Convert.ToString(pi);
        }
    }
}
